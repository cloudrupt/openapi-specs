openapi: 3.0.0
info:
  version: 0.0.1
  title: Watch lists
servers:
  - url: 'https://api.intricately.com/api/v1'
paths:
  /companies/{company_slug}/watch:
    post:
      summary: Adds the given company to the companies watch list
      operationId: addCompany
      tags:
        - Watch lists
      parameters:
        - in: path
          required: true
          name: company_slug
          schema:
            type: string
            example: 'nike'
      responses:
        '201':
          description: Created
          content:
            application/vnd.api+json:
              schema:
                $ref: '#/components/schemas/getWatchListCompany'
        '422':
          $ref: '#/components/responses/Error422'
        '403':
          $ref: '#/components/responses/Error403'
    delete:
      summary: Removes the given company from the companies watch list
      operationId: removeCompany
      tags:
        - Watch lists
      parameters:
        - in: path
          required: true
          name: company_slug
          schema:
            type: string
            example: 'nike'
      responses:
        '204':
          description: No Content
        '403':
          $ref: '#/components/responses/Error403'
        '404':
          $ref: '#/components/responses/Error404'
  /watch_lists/download:
    get:
      summary: Export data for companies in Watch List
      operationId: exportWatchList
      tags:
        - Watch list exports
      parameters:
        - name: fields
          schema:
            type: string
          in: query
          description: A comma-separated list of the fields' that should be added
      responses:
        '200':
          description: Ok, no content
        '400':
          description: 400, bad request
        '402':
          description: 402, monthly download limit exceeded
        '404':
          description: 404, watch list not found
  /watch_list/companies:
    get:
      summary: Lists companies in watch list
      operationId: getWatchListCompanies
      tags:
        - Watch list companies
      parameters:
        - $ref: '#/components/parameters/watchListCompaniesFilters'
        - $ref: '#/components/parameters/watchListCompaniesSorts'
        - $ref: '#/components/parameters/pagination'
      responses:
        '200':
          description: OK
          content:
            application/vnd.api+json:
              schema:
                $ref: '#/components/schemas/getCompanies'
  /watch_list/alert_configuration:
    get:
      summary: Get the watch list configuration
      operationId: getWatchListConfiguration
      tags:
        - Watch list configuration
      responses:
        '200':
          description: OK
          content:
            application/vnd.api+json:
              schema:
                $ref: '#/components/schemas/getWatchListConfiguration'
    put:
      summary: Updates the watch list configuration
      operationId: putWatchListConfiguration
      tags:
        - Watch list configuration
      requestBody:
        content:
          application/vnd.api+json:
            schema:
              $ref: '#/components/schemas/putWatchListConfiguration'
      responses:
        '200':
          description: OK
          content:
            application/vnd.api+json:
              schema:
                $ref: '#/components/schemas/getWatchListConfiguration'
        '400':
          $ref: '#/components/responses/Error400'
        '403':
          $ref: '#/components/responses/Error403'
        '422':
          $ref: '#/components/responses/Error422'
  /watch_list/companies/{company_slug}:
    get:
      summary: Get a watched company
      operationId: getWatchedCompany
      tags:
        - Watch list companies
      parameters:
        - in: path
          name: company_slug
          required: true
          description: The company slug
          schema:
            type: string
            example: 'nike'
      responses:
        '200':
          description: OK
          content:
            application/vnd.api+json:
              schema:
                $ref: '#/components/schemas/getCompany'
        '404':
          $ref: '#/components/responses/Error404'
components:
  responses:
    Error400:
      description: Bad Request
      content:
        application/vnd.api+json:
          schema:
            type: object
            required:
              - errors
            properties:
              errors:
                type: array
                items:
                  required:
                    - status
                    - title
                    - detail
                    - source
                  properties:
                    status:
                      type: string
                      example: '400'
                    title:
                      type: string
                      example: BadRequest
                    detail:
                      type: string
                      example: 'must be one of: all_events, disabled, product_category, product_name'
                    source:
                      type: object
                      required:
                        - pointer
                      properties:
                        pointer:
                          type: string
                          example: '#/data/attributes/attr_name'
    Error403:
      description: Forbidden
      content:
        application/vnd.api+json:
          schema:
            type: object
            properties:
              errors:
                type: array
                items:
                  properties:
                    status:
                      type: string
                      example: '403'
                    detail:
                      type: string
                      example: You can't perform this action
    Error404:
      description: Not Found
      content:
        application/vnd.api+json:
          schema:
            type: object
            required:
              - errors
            properties:
              errors:
                type: array
                items:
                  required:
                    - status
                    - title
                    - detail
                  properties:
                    status:
                      type: string
                      example: '404'
                    title:
                      type: string
                      example: 'Not found'
                    detail:
                      type: string
                      example: Cannot find watch_list_company where slug='nike'
    Error422:
      description: OK
      content:
        application/vnd.api+json:
          schema:
            type: object
            required:
              - errors
            properties:
              errors:
                type: array
                items:
                  required:
                    - status
                    - title
                    - detail
                  properties:
                    status:
                      type: string
                      example: '422'
                    title:
                      type: string
                      example: 'Unprocessable Entity'
                    detail:
                      type: string
                      example: Company already on the watch list
  schemas:
    Company:
      type: object
      required:
        - id
        - type
        - attributes
      properties:
        id:
          type: string
          example: nike
        type:
          type: string
          enum:
            - company
        attributes:
          type: object
          required:
            - name
            - logo
            - industry
            - spend
            - country
            - employee_count
            - na_traffic
            - emea_traffic
            - latam_traffic
            - apj_traffic
            - alexa_rank
          properties:
            name:
              type: string
              example: 'Google'
              nullable: true
            logo:
              type: string
              example: 'https://cdn.intricately.com/logos/google-com/squared.webp'
              nullable: true
              format: uri
            industry:
              type: string
              example: 'Internet'
              nullable: true
            spend:
              type: number
              example: 100000
            country:
              type: string
              example: 'United States'
              nullable: true
            employee_count:
              type: string
              example: '2-10'
              nullable: true
            na_traffic:
              type: number
              example: 0.999
              nullable: true
            emea_traffic:
              type: number
              example: 1.0
              nullable: true
            latam_traffic:
              type: number
              example: 0.005
              nullable: true
            apj_traffic:
              type: number
              example: 0
              nullable: true
            alexa_rank:
              type: integer
              example: 42
              nullable: true
    CompaniesMeta:
      type: object
      required:
        - total_entries
      properties:
        total_entries:
          type: integer
          description: Number of records across all pages
    getWatchListCompany:
      type: object
      required:
        - data
      properties:
        data:
          $ref: '#/components/schemas/WatchListCompany'
    getCompanies:
      type: object
      required:
        - data
        - links
        - meta
      properties:
        data:
          type: array
          items:
            $ref: '#/components/schemas/Company'
        links:
          $ref: '#/components/schemas/PaginationLinks'
        meta:
          $ref: '#/components/schemas/CompaniesMeta'
    getCompany:
      type: object
      required:
        - data
      properties:
        data:
          $ref: '#/components/schemas/Company'
    getWatchListConfiguration:
      type: object
      required:
        - data
      properties:
        data:
          $ref: '#/components/schemas/WatchListAlertConfiguration.Data'
        included:
          type: array
          items:
            anyOf:
              - $ref: '#/components/schemas/WatchListAlertConfigurationFilter.FullData'
    putWatchListConfiguration:
      type: object
      required:
        - data
      properties:
        data:
          type: object
          required:
            - id
            - type
            - attributes
          properties:
            id:
              type: string
              example: 1
            type:
              type: string
              enum:
                - watch_list_alert_configuration_input
            attributes:
              type: object
              required:
                - alert_type
                - filters
              properties:
                alert_type:
                  $ref: '#/components/schemas/WatchListAlertConfiguration.Attributes.AlertType'
                filters:
                  type: array
                  items:
                    type: string
                    example: akamai
    WatchListCompany:
      type: object
      required:
        - type
        - id
        - attributes
      properties:
        type:
          type: string
          enum:
            - watch_list_company
        id:
          type: string
          description: The watch list company id
          example: '1'
        attributes:
          $ref: '#/components/schemas/WatchListCompanyAttributes'
        relationships:
          $ref: '#/components/schemas/WatchListCompanyRelations'
    WatchListCompanyAttributes:
      type: object
      required:
        - created_at
        - updated_at
      properties:
        created_at:
          type: string
          format: date
          description: Timestamp at which the company was added to the watch list
          example: '2020-04-03T00:00:00Z'
        updated_at:
          type: string
          format: date
          description: Timestamp at which the company addition was last updated
          example: '2020-04-03T00:00:00Z'
    WatchListCompanyRelations:
      type: object
      required:
        - company
      properties:
        company:
          type: object
          required:
            - data
          properties:
            data:
              type: object
              required:
                - id
                - type
              properties:
                id:
                  type: string
                  description: The company slug
                  example: 'nike'
                type:
                  type: string
                  enum:
                    - company
    WatchListAlertConfiguration.Data:
      type: object
      required:
        - id
        - type
      properties:
        id:
          type: string
          description: The watch list configuration ID
          example: '1'
        type:
          type: string
          enum:
            - watch_list_alert_configuration
        attributes:
          type: object
          required:
            - alert_type
          properties:
            alert_type:
              $ref: '#/components/schemas/WatchListAlertConfiguration.Attributes.AlertType'
        relationships:
          $ref: '#/components/schemas/WatchListAlertConfiguration.Relationships'
    WatchListAlertConfiguration.Attributes.AlertType:
      type: string
      enum:
        - disabled
        - all_events
        - product_category
        - product_specific
      example: product_specific
    WatchListAlertConfiguration.Relationships:
      type: object
      required:
        - filters
      properties:
        filters:
          type: object
          required:
            - data
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/WatchListAlertConfigurationFilter.Data'
    WatchListAlertConfigurationFilter.Data:
      type: object
      required:
        - id
        - type
      properties:
        id:
          type: string
          example: 1
        type:
          $ref: '#/components/schemas/WatchListAlertConfigurationFilter.Type'
    WatchListAlertConfigurationFilter.FullData:
      allOf:
        - $ref: '#/components/schemas/WatchListAlertConfigurationFilter.Data'
        - type: object
          required:
            - attributes
            - links
          properties:
            attributes:
              type: object
              properties:
                name:
                  type: string
                  example: Akamai
                slug:
                  type: string
                  example: akamai
            links:
              type: object
              properties:
                image:
                  type: string
                  format: uri
                  example: https://S3_LINK.com/product_categories/saas.svg
    WatchListAlertConfigurationFilter.Type:
      type: string
      enum:
        - watch_list_alert_configuration_product_filter
        - watch_list_alert_configuration_service_filter
    PaginationLinks:
      type: object
      properties:
        first:
          example: /path/to/resources?page[number]=10
          format: uri
          type: string
        last:
          example: /path/to/resources?page[number]=10
          format: uri
          type: string
        prev:
          example: /path/to/resources?page[number]=10
          format: uri
          nullable: true
          type: string
        next:
          example: /path/to/resources?page[number]=10
          format: uri
          nullable: true
          type: string
  parameters:
    watchListCompaniesFilters:
      name: filter
      required: false
      in: query
      description: Supported filters
      schema:
        type: object
        properties:
          industry:
            type: string
            example: 'Internet'
          country:
            type: string
            example: 'United States'
          spend_tier:
            type: string
            example: 'a'
            enum:
              - a
              - b
              - c
              - d
              - e
              - f
              - g
            description: The spend tier to filter fore. Allowed values are |
              a -> $500,000+
              b -> $100,000+
              c -> $50,000+
              d -> $20,000+
              e -> $5,000+
              f -> $1,000+
              g -> < $1,000
          employees_tier:
            type: string
            example: 'a'
            enum:
              - a
              - b
              - c
              - d
              - e
              - f
              - g
              - h
              - i
            description: The employes tier to filter fore. Allowed values are |
              a -> 10,001+
              b -> 5,001-10,000
              c -> 1,001-5,000
              d -> 501-1,000
              e -> 201-500
              f -> 51-200
              g -> 11-50
              h -> 2-10
              i -> 1
    watchListCompaniesSorts:
      name: sort
      required: false
      in: query
      description: A comma separated list of attributes to sort by, all attributes are supported
      schema:
        type: string
        example: 'name,industry'
    pagination:
      name: page
      required: true
      in: query
      description: Page number and size
      schema:
        type: object
        required:
          - number
          - limit
        properties:
          number:
            type: integer
          limit:
            type: integer
            description: Number of items per page
