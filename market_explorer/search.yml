openapi: 3.0.0
info:
  version: 0.0.1
  title: Application Insights Applications API
servers:
  - url: 'https://api.intricately.com/api/v1/market_explorer/search/'
paths:
  /market_explorer/search:
    post:
      summary: Search for companies in the dataset
      operationId: marketExplorerSearch
      requestBody:
        content:
          application/vnd.api+json:
            schema:
              type: object
              properties:
                query:
                  $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Query"
                aggregations:
                  $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Aggregations"
      responses:
        "200":
          description: Ok
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/MarketExplorerSearchResponse"
      tags:
        - Market Explorer
components:
  schemas:
    MarketExplorerSearchResponse:
      type: object
      required:
        - total_count
        - data
      properties:
        total_count:
          type: number
          example: 100000
        data:
          type: array
          items:
            type: object
            required:
              - name
              - values
            properties:
              name:
                type: string
                description: aggregation name
                example: products-count-ranges
              values:
                type: array
                items:
                  type: object
                  required:
                    - label
                    - count
                  properties:
                    label:
                      type: string
                      example: 0-1000
                    count:
                      type: number
                      example: 16121473
          example:
            - name: 'products-count-ranges'
              values:
                - label: '0-1000'
                  count: 16121473
                - label: '1000-5000'
                  count: 3206316
                - label: '5000+'
                  count: 1172278
            - name: 'count-by-country'
              values:
                - label: 'United States'
                  count: 957932
                - label: 'China'
                  count: 287117
                - label: 'United Kingdom'
                  count: 184201
    MarketExplorerSearchRequestBody.Query:
      nullable: true
      required:
        - condition
        - rules
      properties:
        condition:
          type: "string"
          enum:
            - and
            - or
          description: Query top-level condition
          example: 'and'
        rules:
          type: array
          description: Rule groups
          items:
            type: object
            required:
              - condition
              - rules
            properties:
              condition:
                type: "string"
                enum:
                  - and
                  - or
                description: Query group-level condition
                example: 'and'
              rules:
                type: array
                description: Rule setting
                items:
                  anyOf:
                    - $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Query.RuleSetting.Exists"
                    - $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Query.RuleSetting.Range"
                    - $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Query.RuleSetting.In"
            example:
              - condition: 'or'
                rules:
                  - key: 'linkedin_url'
                    operator: exists
              - condition: 'or'
                rules:
                  - key: 'total_spend'
                    operator: range
                    value:
                      min: 100000
                      max: 200000
                  - key: 'location.state'
                    operator: in
                    value: ['BR', 'CO']
    MarketExplorerSearchRequestBody.Aggregations:
      nullable: true
      type: array
      items:
        anyOf:
          - $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Aggregations.AggregationSetting.Count"
          - $ref: "#/components/schemas/MarketExplorerSearchRequestBody.Aggregations.AggregationSetting.Range"
    MarketExplorerSearchRequestBody.Query.BaseRuleSetting:
      type: object
      required:
        - key
        - operator
      properties:
        key:
          type: string
          description: Rule Key
          example: total_spend
        operator:
          type: string
          enum:
            - exists
            - range
            - in
    MarketExplorerSearchRequestBody.Query.RuleSetting.Exists:
      allOf:
        - $ref: '#/components/schemas/MarketExplorerSearchRequestBody.Query.BaseRuleSetting'
      properties:
        key:
          example: 'linkedin_url'
          allOf:
            - $ref: '#/components/schemas/MarketExplorer.ExistsRules'
        operator:
          example: exists
    MarketExplorerSearchRequestBody.Query.RuleSetting.Range:
      allOf:
        - $ref: '#/components/schemas/MarketExplorerSearchRequestBody.Query.BaseRuleSetting'
      required:
        - value
      properties:
        key:
          allOf:
            - $ref: '#/components/schemas/MarketExplorer.RangeRules'
        operator:
          example: range
        value:
          type: object
          description: Should define min and max from a range
          required:
            - min
            - max
          properties:
            min:
              type: number
              example: 0
            max:
              type: number
              example: 1000
    MarketExplorerSearchRequestBody.Query.RuleSetting.In:
      allOf:
        - $ref: '#/components/schemas/MarketExplorerSearchRequestBody.Query.BaseRuleSetting'
      required:
        - value
      properties:
        key:
          example: 'location.state'
          allOf:
            - $ref: '#/components/schemas/MarketExplorer.InRules'
        operator:
          example: in
        value:
          type: array
          description: Should define array containing the expected data
          example: ['AL', 'AK']
          items:
            oneOf:
              - type: string
              - type: integer
    MarketExplorerSearchRequestBody.Aggregations.BaseAggregationSetting:
      type: object
      required:
        - key
        - type
      properties:
        key:
          type: string
          description: Aggregation Rule Key
          example: count-by-country
        type:
          type: string
          description: Aggregation type
          enum:
            - count
            - range
    MarketExplorerSearchRequestBody.Aggregations.AggregationSetting.Count:
      allOf:
        - $ref: '#/components/schemas/MarketExplorerSearchRequestBody.Aggregations.BaseAggregationSetting'
      properties:
        key:
          example: count-by-country
          allOf:
            - $ref: '#/components/schemas/MarketExplorer.CountAggregations'
        type:
          example: count
    MarketExplorerSearchRequestBody.Aggregations.AggregationSetting.Range:
      allOf:
        - $ref: '#/components/schemas/MarketExplorerSearchRequestBody.Aggregations.BaseAggregationSetting'
      required:
        - ranges
      properties:
        key:
          example: products-count
          allOf:
            - $ref: '#/components/schemas/MarketExplorer.RangeAggregations'
        type:
          example: ranges
        ranges:
          type: array
          description: Should define set of ranges (from, to)
          items:
            type: object
            required:
              - from
              - to
            properties:
              from:
                type: number
                example: 0
              to:
                type: number
                example: 1000
          example:
            - from: 0
              to: 50
            - from: 50
              to: 200
    MarketExplorer.AvailableRules:
      description: ''
      allOf:
        - $ref: '#/components/schemas/MarketExplorer.ExistsRules'
        - $ref: '#/components/schemas/MarketExplorer.RangeRules'
        - $ref: '#/components/schemas/MarketExplorer.InRules'
    MarketExplorer.ExistsRules:
      type: string
      enum:
        - employees_estimated
        - parent_company
        - linkedin_url
      description: >
        ### Available Exists Rules
          * `employees_estimated` - Filter By `Employee Count is Estimated`
          * `linkedin_url` - Filter By `Linkedin Profile Presence`
          * `parent_company` - Filter By `Parent Record Presence`
    MarketExplorer.RangeRules:
      type: string
      enum:
        - application_count
        - points_of_presence_count
        - alexa_rank
        - spend_ability
        - total_spend
        - estimated_revenue
        - intricately_traffic_rank
        - traffic_growth_qoq
        - traffic_growth_yoy
        - num_products
        - app_workload_score
        - domains_count
        - year_founded
        - record_density_score
        - employee_range
        - spend
        - traffic_distribution_countries.%{COUNTRY_CODE}
        - traffic_distribution.%{REGION_CODE}
      description: >
        ### Available Range Rules
          * `alexa_rank` - Filter by `Alexa Rank`
          * `app_workload_score` - Filter by `Application Workload`
          * `application_count` - Filter by `Application Count`
          * `domains_count` - Filter by `Domains`
          * `employee_range` - Filter by `Employees`
          * `intricately_traffic_#{country}` - Filter by `Traffic Distribution (Country)`
          * `intricately_traffic_#{region}` - Filter by `Traffic Distribution (Region)`
          * `intricately_traffic_rank` - Filter by `Traffic`
          * `num_products` - Filter by `Product Count`
          * `points_of_presence_count` - Filter by `Points of Presence`
          * `record_density_score` - Filter by `Record Completeness`
          * `services.spend` - Filter by `Product Category Spend`
          * `services.vendors.spend` - Filter by `Product Spend`
          * `spend_ability` - Filter by `Spend Potential`
          * `total_spend` - Filter by `Total Spend`
          * `traffic_growth_qoq` - Filter by `Traffic Growth (QoQ)`
          * `traffic_growth_qoy` - Filter by `Traffic Growth (YoY)`
          * `year_founded` - Filter by `Year Founded`
    MarketExplorer.InRules:
      type: string
      enum:
        - industry
        - location
        - use_cases
        - services.shortname
      description: >
        ### Available Inclusion Rules
          * `industry` - Filter By `Industry`
          * `location.country` - Filter By `HQ Country`
          * `location.region` - Filter By `HQ Region`
          * `location.state` - Filter By `HQ State (United States)`
          * `services.shortname` - Filter By `Product Category`
          * `services.vendors` - Filter By `Products`
          * `use_cases` - Filter By `Use Case`
    MarketExplorer.AvailableAggregations:
      allOf:
        - $ref: '#/components/schemas/MarketExplorer.CountAggregations'
        - $ref: '#/components/schemas/MarketExplorer.RangeAggregations'
      description: ''
    MarketExplorer.CountAggregations:
      type: string
      enum:
        - agg_by_employees
        - agg_by_spend_tiers
        - agg_by_spend_potential_tiers
        - agg_by_countries
      description: >
        ### Available Count Aggregations
          * `agg_by_employees` - Group by `Employee Count`
          * `agg_by_spend_tiers` - Group by `Spend Tier`
          * `agg_by_spend_potential_tiers` - Group by `Spend Potential`
          * `agg_by_countries` - Group by `HQ Country`
    MarketExplorer.RangeAggregations:
      type: string
      enum:
        - agg_by_num_applications
      description: >
        ### Available Range Aggregations
          * `agg_by_num_applications` - Group by `App Count`
